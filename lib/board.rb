class Board
  @grid
  @length
  @max_idx

  attr_reader :grid, :length, :max_idx

  def initialize(grid = [Array.new(3), Array.new(3), Array.new(3)])
    @grid = grid
    @length = grid[0].length
    @max_idx = grid[0].length - 1
  end

  def [](pos)
    row, col = pos
    @grid[row][col]
  end

  def []=(pos, mark)
    row, col = pos
    @grid[row][col] = mark
  end

  def place_mark(pos, mark)
    # do nothing if position full
    return if !empty?(pos)
    # place mark
    self[pos] = mark
  end

  def empty?(pos)
    self[pos] == nil ? true : false
  end

  def winner
    # row check
    @grid.each do |row|
      mark = row[0]
      return mark if row.all? do |pos|
        pos == mark && (mark == :X || mark == :O)
      end
    end
    # col check
    (0..2).each do |col|
      mark = @grid[0][col]
      if (1..2).all? do |row|
        @grid[row][col] == mark && (mark == :X || mark == :O)
      end
        return mark
      end
    end
    # left diagonal check
    mark = @grid[0][0]
    if (1..@max_idx).all? do |index|
      @grid[index][index] == mark && (mark == :X || mark == :O)
    end
      return mark
    end
    # right diagonal check
    mark = @grid[0][@max_idx]
    if (1..@max_idx).all? do |index|
      @grid[0 + index][@max_idx - index] == mark && (mark == :X || mark == :O)
    end
      return mark
    end
    # if no win
    nil
  end

  def over?
    # if someone wins, game ends
    return true if winner
    # if no more moves left, game ends
    count = 0
    (0..@max_idx).each do |idx1|
      (0..@max_idx).each do |idx2|
        count += 1 if !empty?([idx1, idx2])
      end
    end
    return true if count == @length**2
    # game is not over yet
    false
  end

end
