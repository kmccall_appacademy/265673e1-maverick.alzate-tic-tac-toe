#
#
#  Game eventually has to allow repeat move if given invalid move
#
#

require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'

class Game
  @board
  @player_one
  @player_two
  @current_player

  attr_accessor :board, :current_player
  attr_reader :player_one, :player_two

  def initialize(player_one, player_two)
    @board = Board.new
    # initialize player_one
    @player_one = player_one
    @player_one.mark = :X
    # initialize player_two
    @player_two = player_two
    @player_two.mark = :O
    @current_player = @player_one
  end

  def play_turn
    # display board
    @current_player.display(@board)
    # make move
    pos = @current_player.get_move
    @board.place_mark(pos, @current_player.mark)
  end

  def game_over
    if @board.over? && @board.winner == @current_player.mark
      puts "#{@current_player.name} won!"
      return true
    elsif @board.over? && !@board.winner
      puts "It's a draw!"
      return true
    end
    false
  end

  def switch_players!
    if @current_player == @player_one
      @current_player = @player_two
    else
      @current_player = @player_one
    end
  end

  def play
    puts "Welcome to tic-tac-toe!"
    loop do
      play_turn
      if !game_over
        switch_players!
      else
        return
      end
    end
  end
end
