class ComputerPlayer
  @name
  @mark
  @board


  attr_reader :name, :board
  attr_accessor :mark

  def initialize(name)
    @name = name
  end

  def display(board)
    @board = board
  end

  def get_move
    # check rows & maybe pick winning move
    (0..@board.max_idx).each do |row|
      if winning_row?(row)
        return winning_row(row)
      end
    end
    # check columns & maybe pick winning move
    (0..@board.max_idx).each do |col|
      if winning_col?(col)
        return winning_col(col)
      end
    end
    # check left diagonal & maybe pick winning move
    if winning_leftdiag?
      return winning_leftdiag
    end
    # check left diagonal & maybe pick winning move
    if winning_rightdiag?
      return winning_rightdiag
    end
    # if no winning move, pick random position
    [Random.rand(@board.max_idx), Random.rand(@board.max_idx)]
  end

  # methods that check & get winning position from row
  def winning_row?(row)
    count = 0
    @board.grid[row].each { |el| count += 1 if el == self.mark }
    count == @board.max_idx ? true : false
  end

  def winning_row(row)
    [row, @board.grid[row].index(nil)]
  end

  # methods that check & get winning position from column
  def winning_col?(col)
    count = 0
    (0..@board.max_idx).each do |idx|
      count += 1 if @board.grid[idx][col] == self.mark
    end
    count == @board.max_idx ? true : false
  end

  def winning_col(col)
    row = 0
    (0..@board.max_idx).each do |idx|
      row = idx if @board.grid[idx][col] == nil
    end
    [row, col]
  end

  # methods that check & get winning position from left diagonal
  def winning_leftdiag?
    count = 0
    (0..@board.max_idx).each do |idx|
      count += 1 if @board.grid[idx][idx] == self.mark
    end
    count == @board.max_idx ? true : false
  end

  def winning_leftdiag
    (0..@board.max_idx).each do |idx|
      return [idx, idx] if @board.grid[idx][idx] == nil
    end
  end

  # methods that check & get winning position from right diagonal
  def winning_rightdiag?
    count = 0
    (0..@board.max_idx).each do |idx|
      count += 1 if @board.grid[0 + idx][@board.max_idx - idx] == self.mark
    end
    count == @board.max_idx ? true : false
  end

  def winning_rightdiag
    (0..@board.max_idx).each do |idx|
      if @board.grid[0 + idx][@board.max_idx - idx] == nil
        return [0 + idx, @board.max_idx - idx]
      end
    end
  end

end
