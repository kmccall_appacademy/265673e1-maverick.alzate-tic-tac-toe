class HumanPlayer
  @name
  @mark
  @board

  attr_reader :name
  attr_accessor :mark

  def initialize(name)
    @name = name
  end

  def display(board)
    # updates board instance
    @board = board
    # displays board
    puts "________"
    @board.grid.each do |row|
      print "| "
      print row.join(" | ")
      print " |\n"
    end
    puts
  end

  def get_move
    puts "Where"
    move = gets.chomp
    move.scan(/\d/).map(&:to_i)
  end
end
